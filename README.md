# A helper library for Advent of code
![java17](https://img.shields.io/badge/java-17+-blue)

A helper library for minimizing the amount of work needed to participate in [https://adventofcode.com](https://adventofcode.com)


### Features

 * Automatic input download
 * Rate limited queries to AoC
 * Local input caching
 * Local correct/wrong solution caching
 * Boilerplate generator
 * Solution speed benchmarks 

 

### Workflow

1. Pre-generate boilerplate for current year and setup the cookies
2. Open the solver template for current day in your text editor
3. Open web browser to read the problem at [https://adventofcode.com](ttps://adventofcode.com)
4. Write the solution and run it to automatically submit the result

## Getting started


For quick start, download this library and add it to your classpath. Then just use the following code. Run it and follow the top section of the printed help.

```java
package mypackage;

import com.github.aoclib.solver.AoC;

public class MyAocSolver {
	public static void main(String[] args){
		DBManager.defaultDB();
		new AoC(args).run();
	}
}
```

Then, run the program again and it tells you what class file you need to create in order to solve a task.

## Full instructions
You may just run the file multiple times and follow instructions after each run. Alternatively, do the following:
### Adding a user

Have the same or similar code as in the example above. 

Then add a user:

`java MyAocSolver user --add myName --cookie session=0123456789...adcdef`

The `myName` is just a local username and is not used to interact with advent of code. While technically multiple usernames and different cookies could be added, there are very few reasons to do so. It's not considered appropriate to solve puzzles for multiple accounts at the same time. Such feature does not exist in this program, nor should it ever be created.


### Creating a solver

To get all default solver boilerplate for a specific year, run: 

`java MyAocSolver tools --generate-year 2023`


#### Alternative: the manual way
Extend class `PuzzleSolver` with a specific name and implement the methods.

The file should be named in the following way;
`solution.yearYYYY.YearYYYYDayDD.java`

Here, `solution.yearYYYY` means the java package, and there should be a file called `YearYYYYDayDD.java`.

For example:
`solution.year2021.Year2021Day06.java`

If you want to store the solvers in some other path, you may setup it like this (before calling `run()` from the solver):

```java
DefaultSolverFactory.setRoot("the.javapacakge.to.files.whatever%d");
```

This path must have a single `%d` which will be replaced by the `--year` argument that is given when solving a specific day.

### Solving a puzzle

Run `java MyAocSolver solve -u myName -y 2021 -d 6`

Note: if the AoC is in progress (i.e. it's December), the year defaults to current year. Otherwise year defaults to previous year. The day defaults to current day (UTC-5). If you are solving the puzzles on the day they unlock, you can just run:

`java MyClass solve -u myName`


### Change the database location

The cache is a single sqlite database file. It will be stored in your working directory as `aoc.db`.

You may set a custom database path. and location by:

```java
DBManager.setFile("path/to/my.db")`
```
Non-existing directories will be generated.


### Customizing SolverFactory

In case you are not happy about initializing the daily solvers by reflection or if you don't like the default naming scheme, feel free to implement `SolverFactory` and set that to the solver by using `myAoC.setSolverFactory(solverFactory);`


At this point, your file main file might look like something like this:

```java
package mypackage;

import com.github.aoclib.solver.AoC;

public class MyClass {
	public static void main(String[] args){
		DBManager.setPath("my/custom/aoc.db");
		new AoC(args)
		  .setSolverFactory(new MyBetterSolverFactory())
		  .run();
	}
}
```


On top of that, this main class file does not really need to be modified. Everything else is done with command line arguments and all the code should be in the daily solver classes. Some advanced exceptions are discussed at the end of this guide.

Pass `-h` to find all the subcommands, and `subcommand -h` to get a specific help.

## Parsing input and solving a puzzle

To actually solve the puzzle, complete the following methods:

```java
public Object firstPart(InputParser input){
  return NOT_SOLVED;
}
public Object secondPart(InputParser input){
  return NOT_SOLVED;
}
```

It's often good idea to just print your result and return `NOT_SOLVED` when you are working on the problem. Better yet, add some proper tests as explained further in this guide. Each returned value that is not considered *invalid*<sup>1</sup> will be submitted to Advent of code, which might get you banned in the long run. The library respects the submission delays imposed by the page, and refuses to submit a same wrong solution twice. After you are sure you want to submit the answer, return the whatever answer you have. Primitives will will be autoboxed and any return value will be automatically converted to string with `toString()`

<sup>1</sup>Some of the invalid values include: `true, false, 0,-1, 1, null, ""` which are either guaranteed to never be the actual answers or are very unlikely to be.


### Input parser examples

`InputParser` contains the input as a `List<String>` and has several methods to get that in a suitable format, such as splitting the lines by a `Delimiter` or by a custom string (regex) and converting each value to any format.

Some examples:

```java
// a single line/word input:
String line =  input.string();

// a single line input that is an integer:
int number = input.integer();

// multiple integers across multiple lines to a single array
// whitespace as default delimiter
int[] myInts = input.asSingleIntArray(); 
int[] myInts2 = input.asSingleIntArray(Delimiter.COMMA); 

// each line of delimited integers as a separate list:
List<List<Integer>> rows = input.linesAsLists(Delimiter.COMMA, Integer::parseInt);

// each group of lines as separate lists.
// Groups of lines are separated by two newlines. 
List<List<String> groups = input.getGroups();

//the input as a single matrix of characters
char[][] myTable = input.charMatrix();
```
See the `InputParser` documentation for the full list of functions. It's possible that all possible or convenient parsers are not implemented, but in that case just use `getlines()` and do the parsing yourself. Adding a custom parser for each day is not the point of this library, as parsing the input can be considered a part of the puzzle each day.

### Adding test cases

The class `PuzzleSolver` has two additional methods that you can override:

```java
protected void insertTestsPart1(List<Test> tests) { }

protected void insertTestsPart2(List<Test> tests) { }
```

These can be used to insert test cases for each part of the puzzle. These have to be manually extracted from the web page. The solver initially calls these methods to get the test cases. Tests cases - if any exist - are run and only if they all pass the actual input will be passed to your function. A partial example implementation for 2015 day 1:

```java
protected void insertTestsPart1(List<Test> tests) {
	tests.add(new Test("(())", 0));
	tests.add(new Test("(()(()(", 3));
	tests.add(new Test("))(", -1));
	tests.add(new Test(")())())", -3));
}

protected void insertTestsPart2(List<Test> tests) {
	tests.add(new Test(")", 1));
	tests.add(new Test("()())", 5));
}
```
Class `Test` requires the test input to be inserted as a single string. Since java 17 you may use multiline strings, i.e. [text blocks](https://docs.oracle.com/en/java/javase/17/text-blocks/index.html).


### Advanced testing

In some puzzles the test cases are explained in the puzzle description in such way that they are directly not part of the input. A regular example would be "after `x` iterations the solution would be `y`". How with the regular test you can only specify the input and the result. 

Consider a trivial puzzle: 
<blockquote>
What is the sum if you add your puzzle input 100 times using a loop?

Some examples:

For input 8 the result is 800

After 10 iterations it should be 80

After 20 iterations it should be 160

</blockquote>
Now if you'd write the solver and tests like this:

```java
public Object firstPart(InputParser input){
  int number = input.integer();
  int sum = 0;
  // yes, this task does not need a loop,
  // it's a simple multiplication.
  for(int i = 0;i < 100;i++){
    sum += number;
  }
  return sum;
}
protected void insertTestsPart1(List<Test> tests) {
	tests.add(new Test("8", 800));
}
```


You have the constant `100` that is given within the task, but some test cases have another constant that is not part of the actual input. This can be done like so:

```java
//implement the default constructor and inject named constants
public Year9999Day99(){
  part1constants("iterations",100);
  part2constants("something else",-1); // same for part 2
}

public Object firstPart(InputParser input){
  int number = input.integer();
  int sum = 0;
  int iterations = input.intConstant("iterations");

  for(int i = 0;i < iterations; i++){
    sum += number;
  }
  return sum;
}

protected void insertTestsPart1(List<Test> tests) {
	// now it works!
	tests.add(new Test("8", 80, Map.of("iterations",10)));
	tests.add(new Test("8", 160, Map.of("iterations",20)));
	tests.add(new Test("8", 800)); // this uses the defaults
}
```

If constants are not given in the test case, the default constants specified in the constructor will be used.


### Other debugging stuff


In case you have a problem where you just want to run the tests, but not with the actual inputs, you can set

`ONLY_TEST = true;`

at any point in the solver, this does not run with the actual data. This might be useful when for example:

* Tests succeed but the main input produces clearly wrong results all the time and you don't want to submit it
* When  adding custom tests that are still in progress and might succeed by accident.
* When the code is unoptimized and would take hours, days, years to run on the actual input.


If the program fails to report the actual problem or seems to parse the html responses in a weird way, add `AOCApi.saveReponses = true;` in the `main`-method. This saves the html responses and you can drop an issue to my gitlab page. So far this does not save the html request though, so make sure you know what you submitted to cause that reply.