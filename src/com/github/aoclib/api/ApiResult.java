package com.github.aoclib.api;

import java.util.Optional;

public class ApiResult {
    
    private Optional<Integer> delaySeconds;
    
    
    
    public int getDelay() {
	return delaySeconds.orElse(0);
    }
}
