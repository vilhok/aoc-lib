package com.github.aoclib.api;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Represents a response given by https://adventofcode.com
 */
public class AOCResponse {

    private final String contents;

    public AOCResponse(String contents) {
	this.contents = contents;
    }

    /**
     * The returned body as a string
     * 
     * @return
     */
    public String raw() {
	return contents;
    }

    /**
     * Parses and returns the meaningful bit from the result.
     * 
     * @return
     */
    public SubmitStatus parseStatus() {
	return SubmitStatus.forResult(contents);
    }

    public ApiResult parseResult() {
	Document d = Jsoup.parse(contents);
	Elements eList = d.getElementsByTag("main");
	
	Element e = eList.get(0);
	String s = e.outerHtml();
	System.out.println(s);
	return null;
    }
    // TODO: return an actual result instead, status it
}
