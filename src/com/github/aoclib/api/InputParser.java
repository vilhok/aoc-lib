package com.github.aoclib.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.github.aoclib.utils.Delimiter;
import com.github.aoclib.utils.Instruction2017;

public class InputParser {

    private Map<String, Object> constants;

    private final List<String> input;

    public InputParser(List<String> input) {
	this.input = input;
	constants = new HashMap<>();
    }

    public void setConstants(Map<String, Object> constants) {
	this.constants = constants;
    }


    public int intConstant(String key) {
	return (int)constants.get(key);
    }
    
    public String stringConstant(String key) {
  	return (String)constants.get(key);
      }

    public int firstLineLength() {
	return input.get(0).length();
    }

    /**
     *
     * Returns true if all the input lines have the same length. Lengths are
     * compared by {@link String#length()}.
     * 
     * @return Are all the input lines equally long
     */
    public boolean allSameLength() {
	int len = firstLineLength();
	return input.stream().mapToInt(String::length).allMatch(i -> i == len);

    }

    public InputParser(String inputString) {
	this.input = new ArrayList<>(Arrays.asList(inputString.split("\n")));

    }

    /**
     * Join all the lines to a single string, without doing any splitting.
     * 
     * @return A single string containing all the rows
     */
    public String string() {
	return joinLinesToString(Delimiter.NONE);

    }

    /**
     * Returns the single and supposedly only line as an int.
     * 
     * @return the first line of the input as an int.
     */
    public int integer() {
	return Integer.parseInt(input.get(0));

    }

    /**
     * Combines all lines and returns then as char array
     * 
     * @return
     */
    public char[] chars() {
	return joinLinesToString(Delimiter.NONE).toCharArray();
    }

    /**
     * Return lines as unprocessed strings
     * 
     * @return All rows of the input as a List
     */
    public List<String> getLines() {
	return Collections.unmodifiableList(input);
    }

    public List<String> getLinesSkipFirst() {
	return Collections.unmodifiableList(input.subList(1, input.size()));
    }

    public <T> List<T> getLines(Function<String, T> mapper) {
	return input.stream().map(mapper).toList();
    }

    public <T, R extends Collection<T>> R getLines(Function<String, T> mapper, R container) {
	input.stream().map(mapper).forEach(container::add);
	return container;
    }

    /**
     * Return groups of lines as separate lists.
     * 
     * A group is defined by multiple lines, separated by newlines.
     * 
     * @return Lists of Strings for each group, contained in a List
     */
    public List<List<String>> getGroups() {
	return getGroups(Object::toString);
    }

    /**
     * Return groups of lines as separate lists.
     * 
     * A group is defined by multiple lines, separated by newlines.
     * 
     * @return Lists of Strings for each group, contained in a List
     */
    public <T> List<List<T>> getGroups(Function<String, T> mapper) {
	List<List<T>> list = new ArrayList<>();
	ArrayList<T> current = new ArrayList<>();
	for (String line : input) {
	    if (line.isBlank()) {
		if (!current.isEmpty()) {
		    list.add(current);
		}
		current = new ArrayList<>();
	    } else {
		current.add(mapper.apply(line));
	    }
	}
	// adds the final group if there was no blank line as the last line
	if (list.get(list.size() - 1) != current) {
	    list.add(current);
	}
	return list;
    }

    /**
     * Split each row by Delimiter and return then in a string array of strings.
     * 
     * @param valueSeparator
     * @return
     */
    public String[] joinLineValuesToArray(Delimiter valueSeparator) {

	ArrayList<String> str = new ArrayList<>();
	for (String s : input) {
	    String[] a = s.split(valueSeparator.value);
	    for (String ss : a) {
		str.add(ss.trim());
	    }
	}
	return str.toArray(new String[0]);
    }

    /**
     * Returns the data as a single string. All lines are combined by using a
     * specific delimiter.
     * 
     * @param delimiter
     * @return
     */
    public String joinLinesToString(Delimiter delimiter) {
	return input.stream().collect(Collectors.joining(delimiter.value));
    }

    /**
     * Returns whitespace separated integers
     * 
     * @return
     */
    public int[] asSingleIntArray() {
	return asSingleIntArray(Delimiter.WHITESPACE);
    }

    /**
     * Returns a single integer array from single line of input by using a custom
     * delimiter
     * 
     * @param rowValueDelimiter
     * @return
     */
    public int[] asSingleIntArray(Delimiter rowValueDelimiter) {
	List<String> lines = input;
	return lines.stream() //
		.filter(s -> !s.matches("\\s*")) //
		.flatMap(s -> Arrays.stream(s.split(rowValueDelimiter.value))) //
		.mapToInt(Integer::parseInt) //
		.toArray();
    }

    public long[] asSingleLongArray(Delimiter splitRegex) {
	List<String> lines = input;
	return lines.stream() //
		.filter(s -> !s.matches("\\s*")) //
		.flatMap(s -> Arrays.stream(s.split(splitRegex.value))) //
		.mapToLong(Long::parseLong) //
		.toArray();
    }

    /**
     * Returns each split line as a list of items of type T. Requires a custom
     * mapper from String to T
     * 
     * @param <T>
     * @param sep
     * @param valueMapper
     * @return
     */
    public <T> List<List<T>> linesAsLists(String sep, Function<String, T> valueMapper) {

	return input //
		.stream()//
		.map(String::trim)//
		.filter(line -> !line.isBlank())//
		.map(i -> Arrays.stream(i.split(sep))//
			.map(valueMapper)//
			.toList())
		.toList();
    }

    public <T> List<List<T>> linesAsLists(Delimiter sep, Function<String, T> valueMapper) {
	return linesAsLists(sep.value, valueMapper);
    }

    /**
     * Returns a matrix with each line split to char array.
     * 
     * @return char[][] where that contains each row as char[]
     */
    public char[][] charMatrix() {
	return input.stream()//
		.map(String::toCharArray)//
		.toList()//
		.toArray(char[][]::new);

    }

    /**
     * Returns a matrix as an integer array. Assume the input consists of strings
     * that have only ascii numbers
     * 
     * 
     */
    public int[][] intMatrix() {
	int[][] in = new int[input.size()][];
	int index = 0;
	for (String line : input) {
	    in[index++] = line.chars().map(i -> i - 48).toArray();
	}

	return in;
    }

    /**
     * Returns each line as separated values.
     * 
     * @param rowValueDelimiter the delimiter for values on each row.
     * @return A list containing a list for eachs rows values.
     */
    public List<List<String>> linesAsLists(Delimiter rowValueDelimiter) {
	return linesAsLists(rowValueDelimiter.value, Object::toString);
    }

    public List<List<String>> linesAsLists(String rowValueDelimiter) {
	return linesAsLists(rowValueDelimiter, Object::toString);
    }

    /**
     * Returns the input as 2019 intcode program
     * 
     * @return a long[] array that represent the intcode program
     */
    public long[] intCodeProgram() {
	return asSingleLongArray(Delimiter.COMMA);
    }

    /**
     * Get as year 2017 instructions
     * 
     * @param instructionDelimiter
     * @return
     */
    public List<Instruction2017> as2017Instruction(Delimiter instructionDelimiter) {
	return input.stream().map(e -> new Instruction2017(e, instructionDelimiter)).collect(Collectors.toList());
    }

}
