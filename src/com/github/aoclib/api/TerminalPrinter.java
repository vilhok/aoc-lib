package com.github.aoclib.api;

public class TerminalPrinter {

    public static final Ansi ESCAPE = new Ansi("\u001B[");

    public static final Ansi YELLOW = new Ansi(ESCAPE + "33m");
    public static final Ansi RED = new Ansi(ESCAPE + "31m");
    public static final Ansi GREEN = new Ansi(ESCAPE + "32m");

    public static final Ansi BOLD = new Ansi(ESCAPE + "1m");

    public static final Ansi ITALIC = new Ansi(ESCAPE + "3m");
    public static final Ansi UNDERLINE = new Ansi(ESCAPE + "4m");

    public static final Ansi RESET = new Ansi(ESCAPE + "0m");

    public static boolean enabled = true;

    public static class Ansi {
	private String code;

	public Ansi(String code) {
	    this.code = code;
	}

	@Override
	public String toString() {
	    return code;
	}
    }

    public static String format(Object o, Ansi... codes) {
	StringBuilder sb = new StringBuilder();
	if (enabled)
	    for (Ansi a : codes) {
		sb.append(a.toString());
	    }
	sb.append(o.toString());
	if (enabled)
	    sb.append(RESET);
	return sb.toString();
    }

    public static void print(Object o, Ansi... codes) {
	if (enabled)
	    for (Ansi a : codes) {
		System.out.print(a);
	    }
	System.out.print(o);
	if (enabled)
	    System.out.print(RESET);
    }

    public static void println(Object o, Ansi... codes) {
	print(o, codes);
	System.out.println();
    }
}
