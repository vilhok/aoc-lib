package com.github.aoclib.utils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Instruction2017 {

    private List<String> values;
    public String line;

    public Instruction2017(String line, Delimiter d) {
	this.line = line;
	values = Arrays.stream(line.split(d.value)).collect(Collectors.toList());
    }

    public String name() {
	return values.get(0);
    }

    public void setArg(int i, String val) {
	values.set(i, val);
    }

    public String arg(int i) {
	if (values.size() < i) {
	    return "";
	}
	return values.get(i + 1);
    }

    @Override
    public String toString() {
	return values.toString();
    }
}
