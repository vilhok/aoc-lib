package com.github.aoclib.solver;

public class SolverParameters {

	private int year;
	private int day;

	public SolverParameters(int year, int day) {
		this.year = year;
		this.day = day;
	}

	public int getYear() {
		return year;
	}

	public int getDay() {
		return day;
	}
}
