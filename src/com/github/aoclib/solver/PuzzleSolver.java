package com.github.aoclib.solver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import com.github.aoclib.api.InputParser;
import com.github.aoclib.api.TerminalPrinter;

/*
 * interface for all days
 */
public abstract class PuzzleSolver {

    /*
     * Default return value for any day. Returning this from the solver guarantees
     * that the solution will not be pushed to AoC server.
     */
    public static final String NOT_SOLVED = "NOT_SOLVED";

    /**
     * Set this to true from the subclass, if you only want to run tests even if
     * they succeed.
     */

    protected boolean ONLY_TEST = false;

    /*
     * Consider a test to pass, if it returns NOT_SOLVED
     */
    protected boolean NOT_SOLVED_IS_PASSED = false;

    /**
     * 
     */
    private InputParser input;
    private SolverParameters params;

    /*
     * Injects the parameters to clean up the actual solution classes (no
     * constructor needed). Not sure if I like this, but it works for this project.
     */
    public void setup(SolverParameters params, InputParser input) {
	this.params = params;
	this.input = input;
    }

    private Map<String, Object> defaultTestConstants = Collections.unmodifiableMap(Map.ofEntries());

    private Map<String, Object> part1Constants = new HashMap<>();
    private Map<String, Object> part2Constants = new HashMap<>();

    public void part1Constant(String key,Object value) {
	part1Constants.put(key, value);
    }
    public void part2Constant(String key,Object value) {
	part2Constants.put(key, value);
    }
    
    /**
     * Represents a single test case with an input and expected solution
     */
    public class Test {

	private String testInput;
	private Object expectedSolution;

	private Map<String, Object> constants;

	public Test(String testInput, Object expectedSolution) {
	    this(testInput, expectedSolution, defaultTestConstants);
	}

	public Test(String testInput, Object expectedSolution, Map<String, Object> constants) {
	    this.testInput = testInput;
	    this.expectedSolution = expectedSolution;
	    this.constants = constants;
	}
    }

    /**
     * Tests for part 1
     * 
     * Add tests to your solution as following:
     * 
     * {@code new Test(multiLineStringInput,solutionString)}
     * 
     * These tests will run before your actual input. If any test fails, the actual
     * data is not used.
     * 
     * @param tests a list where tests should be inserted..
     */
    protected void insertTestsPart1(List<Test> tests) {

    }

    /**
     * Tests for part 2
     * 
     * Add tests to your solution as following:
     * 
     * {@code new Test(multiLineStringInput,solutionString)}
     * 
     * These tests will run before your actual input. If any test fails, the actual
     * data is not used.
     * 
     * @param tests a list where tests should be inserted..
     */
    protected void insertTestsPart2(List<Test> tests) {

    }

    /**
     * Implement these to submit your solutions
     */
    @SuppressWarnings("unused")
    protected abstract Object firstPart(InputParser ip);

    protected abstract Object secondPart(InputParser ip);

    public Solution solveFirstPart() {
	long time = System.nanoTime();
	input.setConstants(part1Constants);
	Object s = firstPart(input);
	long runtime = System.nanoTime() - time;
	String solution = s == null ? "null" : s.toString();
	return new Solution(params.getYear(), params.getDay(), Part.FIRST, solution, runtime);
    }

    public Solution solveSecondPart() {
	long time = System.nanoTime();
	input.setConstants(part2Constants);
	Object s = secondPart(input);
	long runtime = System.nanoTime() - time;
	String solution = s == null ? "null" : s.toString();
	return new Solution(params.getYear(), params.getDay(), Part.SECOND, solution, runtime);
    }

    /**
     * Return true in either of following cases: <br>
     * -No tests are given. <br>
     * -All tests pass
     * 
     * @return true if tests pass
     */
    public boolean testFirstPart(int[] skiptests) {
	ArrayList<Test> tests = new ArrayList<>();
	insertTestsPart1(tests);
	if (tests.size() > 0)
	    TerminalPrinter.println("Testing part 1:", TerminalPrinter.BOLD);
	return performTests(tests, this::firstPart, part1Constants);
    }

    /**
     * Return true in either of following cases: <br>
     * -No tests are given. <br>
     * -All tests pass
     * 
     * @return true if tests pass
     */
    public boolean testSecondPart(int[] skiptests) {
	ArrayList<Test> tests = new ArrayList<>();
	insertTestsPart2(tests);
	if (tests.size() > 0)
	    TerminalPrinter.println("Testing part 2:", TerminalPrinter.BOLD);
	return performTests(tests, this::secondPart, part2Constants);
    }

    private boolean performTests(List<Test> tests, Function<InputParser, Object> solverFunction,
	    Map<String, Object> defaultConstants) {
	int notSolvedCnt = 0;
	int index = 0;
	boolean allPassed = true;
	for (Test tc : tests) {
	    InputParser input = new InputParser(tc.testInput);
	    if (tc.constants == defaultTestConstants) {
		input.setConstants(defaultConstants);
	    } else {
		input.setConstants(tc.constants);
	    }
	    TestSolution result = new TestSolution(solverFunction.apply(input).toString());
	    if (result.solution.equals(NOT_SOLVED)) {
		notSolvedCnt++;
		allPassed = false;

	    }
	    if (!result.solution.equals(tc.expectedSolution.toString())) {
		System.out
			.println("\t" + TerminalPrinter.format("Test " + (index + 1) + " failed ", TerminalPrinter.RED)
				+ "(" + result.solution + ")"
				+ TerminalPrinter.format(" Expected: ", TerminalPrinter.BOLD) + tc.expectedSolution);
		allPassed = false;
	    } else {
		System.out.println("\t" + TerminalPrinter.format("Test " + (index + 1) + " passed ",
			TerminalPrinter.GREEN, TerminalPrinter.BOLD) + "(" + result.solution + ")");
	    }
	    index++;
	}

	if (notSolvedCnt == tests.size() && NOT_SOLVED_IS_PASSED) {
	    System.err.println(">>Warning: " + notSolvedCnt + "/" + tests.size() + " tests returned NOT_SOLVED");
	    System.err.println(">>NOT_SOLVED_IS_PASSED is true, therefore this means all tests passed");
	    return true;

	}
	return allPassed;
    }

    public boolean onlyTest() {
	return ONLY_TEST;
    }
}
