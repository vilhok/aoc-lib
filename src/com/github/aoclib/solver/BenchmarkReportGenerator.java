package com.github.aoclib.solver;

public class BenchmarkReportGenerator {
	int lineWidth = 45;

	class YearReport {

	}

	String header = "# Solution benchmarks";

	String notes = """
			Note about runtime: Java does heavy runtime optimization during the execution.
			If you run a single day once, the second part might be much faster even though
			the algorithm is longer. This is due to fact that for example the input parsers
			methods are JITted or otherwise optimized and the second call is much faster.

			This is apparent if you run a benchmark once for the whole year as well.
			A solution that originally took some milliseconds, might be done in microseconds
			during subsequent executions.

			Depending on what you are looking for, a daily solution ran alone from cold start
			might be the most meaningful metric. But in case of java this is often a bit
			useless metric, as there are huge variations. Therefore the default benchmark
			report does several runs first, before starting to collect actual running times.
			Even then, the runtimes are subject to variations caused by garbage collection.

			You may try to affect this by changing JVM options.

			""";

}
